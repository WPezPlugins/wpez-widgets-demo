<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace  WPezWidgetsDemo\App\Core\WidgetAbstract;

abstract class AbstractUpdate {

	protected $_container;
	protected $_this_widget;
	protected $_widget_instance_new;
	protected $_widget_instance_old;

	public function __construct( $container = false ) {

		$this->_container = $container;

	}

	protected function getDefaults(){

		return $arr = [
			'active' => true,   // remove? unnecessary?
			'inst_new' => $this->_widget_instance_new,
			'inst_old' => $this->_widget_instance_old,
			'this'  => $this->_this_widget,
		];
	}

	public function setInstanceNew( $instance ) {

		$this->_widget_instance_new = $instance;

	}

	public function setInstanceOld( $instance ) {

		$this->_widget_instance_old = $instance;

	}

	public function setThisWidget(\WP_Widget $this_widget){

		$this->_this_widget = $this_widget;

	}

	abstract public function setUpdate();

}