<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetGetValues;

trait TraitGetNumber {

	public function getNumber( $arr_args = false ) {

		// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/number
		$arr_defaults = [
			'min'  => false,
			'max'  => false,
			'step' => '1',
			'size' => '3',
			'cast' => 'default'
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! is_array( $arr['inst_new'] ) ) {
			return false;
		}
		if ( ! is_string( $arr['name'] ) ) {
			return false;
		}

		$mix_value = null;

		// if we need a default value, which should we use?
		if ( ! isset( $arr['inst_new'][ $arr['name'] ] ) ) {

			if ( isset( $arr['default'] ) ) {
				$mix_value = $arr['default'];
			} elseif ( isset( $arr['min'] ) && $arr['min'] !== false ) {
				$mix_value = $arr['min'];
			} elseif ( isset( $arr['max'] ) && $arr['max'] !== false ) {
				$mix_value = $arr['max'];
			} else {
				$mix_value = 0;
			}
		}

		if ( $mix_value === null ) {

			$mix_value = $arr['inst_new'][ $arr['name'] ];
		}

		// TODO - check for min / max, and valid step

		switch ( $arr['cast'] ) {

			case 'int':
				return (integer) $mix_value;

			case 'float':
				return (float) $mix_value;

			default: // 'absint'
				return absint( $mix_value );

		}

	}
}