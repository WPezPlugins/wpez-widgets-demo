<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetGetValues;

trait TraitAllGetValues {

	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetBool;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetMultiForOptions;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetNumber;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetValueElseDefault;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetValueForOptions;

}
