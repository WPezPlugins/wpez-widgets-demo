<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetGetValues;

trait TraitGetMultiForOptions {

	public function getMultiForOptions( $arr_args = false ) {

		$arr_defaults = [
			'inst_new' => false,
			'inst_old' => false,
			'name'     => false,
			'options' => [],
			'default'  => []
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! is_array($arr['inst_new']) ){
			return false;
		}
		if ( ! is_string( $arr['name']) ){
			return false;
		}

		if ( ! is_array($arr['options']) ){
			if ( ! is_array($arr['default'])) {
				return [$arr['default']];
			} else {
				return $arr['default'];
			}
		}

		if ( isset( $arr['inst_new'][ $arr['name']]) && is_array( $arr['inst_new'][ $arr['name']])
		     && isset( $arr['options']) && is_array( $arr['options']) ){

			return array_intersect( array_keys($arr['options']), $arr['inst_new'][ $arr['name']]);

		}
		if ( ! is_array($arr['default'])) {
			return [$arr['default']];
		} else {
			return $arr['default'];
		}

	}
}