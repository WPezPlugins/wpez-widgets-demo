<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetGetValues;

trait TraitGetBool {

	public function getBool( $arr_args = false ) {

		$arr_defaults = [
			'inst_new' => false,
			'inst_old' => false,
			'name'     => false,
			'sanitize' => 'strip_tags',
			'default'  => true
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! is_array($arr['inst_new']) ){
			return false;
		}
		if ( ! is_string( $arr['name']) ){
			return false;
		}

		// TODO - Allow checkbox to be checked as default
		return ( isset( $arr['inst_new'][ $arr['name'] ] ) ) ? ( (bool)( $arr['inst_new'][ $arr['name'] ] ) ) : false;




	}
}