<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetFormElements;

trait TraitAllFormElements {

	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitCheckBool;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitCheckMulti;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitInput;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitRadio;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitSelect;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitTextarea;

}