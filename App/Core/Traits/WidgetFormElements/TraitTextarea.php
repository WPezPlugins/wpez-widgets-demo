<?php

namespace WPezWidgetsDemo\App\Core\Traits\WidgetFormElements;

trait TraitTextarea{

	public function widgetTextarea( $arr_args = false  ){

		if ( ! is_array($arr_args) ){
			return false;
		}

		$arr_defaults =[
			'this' => false,
			'label' => false,
			'name' => false,
			'class' => 'widefat',
			'type' => false,
			'value' => '',
			'placeholder' => false,
		];

		$arr = array_merge($arr_defaults, $arr_args);

		if ( ! $arr['this'] instanceOf \WP_Widget) {
			return false;
		}

		if ( ! is_string($arr['name']) ){
			return false;
		}

		if ( empty($arr['name'] )){
			return false;
		}

		if ( $arr['type'] !== 'textarea' ){
			return false;
		}

		if ( ! is_string($arr['class']) ){
			$arr['class'] = 'widefat';
		}

		if ( ! is_string($arr['placeholder']) ){
			$arr['placeholder'] = '';
		}

		$str_ret = '';
		if ( is_string( $arr['label'] ) ) {
			$str_ret .= '<label for="' . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '">';
			$str_ret .= esc_attr( $arr['label'] );
			$str_ret .= '</label> ';
		}

		$str_ret .= '<textarea';
		$str_ret .= ' id="'  . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '"';
		$str_ret .= ' class="' . esc_attr( $arr['class']) . '"';
		$str_ret .= ' name="' . esc_attr( $arr['this']->get_field_name( $arr['name'] ) ) . '"';
		$str_ret .= ' placeholder="' . esc_attr($arr['placeholder']) . '"';
		$str_ret .= '>';
		$str_ret .= esc_textarea( $arr['value']);
		$str_ret .= '</textarea>';

		echo $str_ret;

	}
}
