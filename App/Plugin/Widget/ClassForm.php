<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace WPezWidgetsDemo\App\Plugin\Widget;

use WPezWidgetsDemo\App\Core\WidgetAbstract\AbstractForm;

class ClassForm extends AbstractForm {


	public function getView() {

		$arr_widget_fields = $this->_container->settings->fields;

		//	var_dump( $arr_widget_fields );

		if ( ! is_array( $arr_widget_fields ) || empty( $arr_widget_fields ) ) {
			return '';
		}

		$get_values    = $this->_container->get_values;
		$form_elements = $this->_container->form_elements;

		$str_key = 'title';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getValueElseDefault( $arr );

			echo '<p>';
			$form_elements->widgetInput( $arr );
			echo '</p>';
		}

		$str_key = 'test_chk_bool';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getBool( $arr );

			echo '<p>';
			$form_elements->widgetCheckbool( $arr );
			echo '</p>';
		}

		$str_key = 'test_chk_multi';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getMultiForOptions( $arr );

			echo '<p>';
			$form_elements->widgetCheckMulti( $arr );
			echo '</p>';
		}

		$str_key = 'test_number_1';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getNumber( $arr );

			echo '<p>';
			$form_elements->widgetInput( $arr );
			echo '</p>';
		}

		$str_key = 'test_radio';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getValueForOptions( $arr );

			echo '<p>';
			$form_elements->widgetRadio( $arr );
			echo '</p>';
		}

		$str_key = 'test_select';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getValueForOptions( $arr );

			echo '<p>';
			$form_elements->widgetSelect( $arr );
			echo '</p>';
		}

		$str_key = 'test_textarea';
		$arr     = array_merge( $this->getDefaults(), $arr_widget_fields[ $str_key ] );
		if ( $arr['active'] === true ) {
			$arr['value'] = $get_values->getValueElseDefault( $arr );

			echo '<p>';
			$form_elements->widgetTextarea( $arr );
			echo '</p>';
		}

	}
}