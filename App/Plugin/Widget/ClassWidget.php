<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 9:09 AM
 */

namespace WPezWidgetsDemo\App\Plugin\Widget;

use WPezWidgetsDemo\App\Core\WidgetAbstract\AbstractWidget;


class ClassWidget extends AbstractWidget {

	public function getView() {

		$container = $this->_container;
		$inst = $this->_widget_instance;
		$args = $this->_widget_args;

		echo '<h1>' . 'The frontend is now totally up to you' . '</h1>';

	}

}