<?php

namespace WPezWidgetsDemo\App\Plugin\Widget;

class ClassWidgetsInit {

	protected $_container;


	public function __construct( $container = false ) {

		$this->_container = $container;

	}


	public function registerWidget() {

		$new_settings = $this->_container->settings;
		$new_wpwidget = new ClassWPWidgetDemo( $new_settings->widget_args );

		$new_wpwidget->setNewAll( new ClassForm( $this->_container ), new ClassUpdate( $this->_container ), new ClassWidget( $this->_container ) );

		register_widget( $new_wpwidget );

	}

}