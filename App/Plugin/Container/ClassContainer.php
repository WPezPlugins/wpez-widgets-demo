<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/1/2019
 * Time: 10:28 AM
 */

namespace WPezWidgetsDemo\App\Plugin\Container;

use WPezSuite\WPezCore\Bases\Language;
use WPezWidgetsDemo\App\Plugin\Container\Contents\ClassSettings as Settings;
use WPezWidgetsDemo\App\Plugin\Container\Contents\ClassFormElements as FormElements;
use WPezWidgetsDemo\App\Plugin\Container\Contents\ClassGetValues as GetValues;

class ClassContainer {

    protected $_new_settings;
    protected $_new_form_elements;
    protected $_new_get_values;

    public function __construct() {

    	$this->setPropertyDefaults();
    }

    protected function setPropertyDefaults(){

	    $this->_new_settings = false;
	    $this->_new_form_elements = false;
	    $this->_new_get_values = false;
    }


    public function __get($str_request){

        switch($str_request){

	        case 'form_elements':
	        	return $this->getFormElements();

	        case 'get_values':
	        	return $this->getGetValues();

	        case 'settings':
		        return $this->getSettings();

            default:
                return false;
        }
    }

    protected function getSettings(){

        if ( ! is_object($this->_new_settings )){
            $this->_new_settings = new Settings();
        }
        return $this->_new_settings;
    }

	protected function getFormElements(){

		if ( ! is_object($this->_new_form_elements )){
			$this->_new_form_elements = new FormElements();
		}
		return $this->_new_form_elements;
	}

	protected function getGetValues(){

		if ( ! is_object($this->_new_get_values )){
			$this->_new_get_values = new GetValues();
		}
		return $this->_new_get_values;
	}


}