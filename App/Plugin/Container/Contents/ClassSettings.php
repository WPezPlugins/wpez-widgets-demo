<?php

namespace WPezWidgetsDemo\App\Plugin\Container\Contents;


class ClassSettings {


	protected $_arr_fields;
	protected $_arr_widget_args;


	public function __construct() {

		$this->setProperyDefaults();
	}

	public function __get( $str_prop = false ) {

		switch ( $str_prop ) {

			case 'widget_args':
				return $this->_arr_widget_args;

			case 'fields':
			default:
				return $this->_arr_fields;

		}

	}

	protected function setProperyDefaults() {

		$this->_arr_widget_args = [

				//	'base_id' => 'my_base_id',
				'title'       =>  __( 'WPezWidgets Demo', ' wpez-widgets' ),
				//'classname' => 'wpez-gfgw-cta',
				'description' =>  __( 'This is the widget description.', ' wpez-widgets' ),
		];

		$this->_arr_fields = [

			'title' => [
				'label' => __( 'Standard Title', ' wpez-widgets' ),
				'name'  => 'title',
				// 'class' => 'widefat',
				'type'  => 'text',
				//	'value' => '',
				//	'sanitize' => 'strip_tags',
				//	'default' => ''
			],

			'test_chk_bool' => [
				'label' => __( 'Check Bool', ' wpez-widgets' ),
				'name'  => 'test_chk_bool',
				'type'  => 'chk_bool'
			],


			'test_chk_multi' => [
				'label'   => __( 'Check Multi', ' wpez-widgets' ),
				'name'    => 'test_chk_multi',
				'type'    => 'chk_multi',
				'options' => [
					'chk_one'   => 'Chk One',
					'chk_two'   => 'Chk Two',
					'chk_three' => 'Chk Three',
					'chk_four'  => 'Chk Four',
					'chk_five'  => 'Chk Five'
				],
				'default' => 'chk_two'
			],

			'test_number_1' => [
				'label'   => __( 'Number', ' wpez-widgets' ),
				'name'    => 'test_number_1',
				'type'    => 'number',
				'class' => 'tiny-text',
				'min'     => '-5',
				'max'     => '5',
				'step'    => '0.5',
				'default' => '1.5',
				'cast'    => 'float'
			],

			'test_radio' => [
				// note: for radio the label becomes the fieldset legend
				'label'   => __( 'Radio', ' wpez-widgets' ),
				'name'    => 'test_radio',
				'type'    => 'radio',
				'br'      => false,
				'options' => [
					'one'   => 'One',
					'two'   => 'Two',
					'three' => 'Three'
				],
				'default' => 'two'
			],

			'test_select' => [
				'label'   => __( 'Select', ' wpez-widgets' ),
				'name'    => 'test_select',
				'type'    => 'select',
				'options' => [
					'one'   => 'One',
					'two'   => 'Two',
					'three' => 'Three',
					'four'  => 'Four',
					'five'  => 'Five'
				],
				'default' => 'two'
			],

			'test_textarea' => [
				'label' => __( 'Textarea', ' wpez-widgets' ),
				'name'  => 'test_textarea',
				// 'class' => 'widefat',
				'type'  => 'textarea',
				//	'value' => '',
				//	'sanitize' => 'strip_tags',
				//	'default' => ''
			],

		];


	}
}