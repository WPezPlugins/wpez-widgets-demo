<?php

namespace WPezWidgetsDemo\App\Plugin\Container\Contents;


class ClassFormElements {

	// Note: You can use the All trait, or use them individually and comment out the ones you're going to use for your widget

	// use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitAllFormElements;


	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitCheckBool;
	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitCheckMulti;
	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitInput;
	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitRadio;
	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitSelect;
	use  \WPezWidgetsDemo\App\Core\Traits\WidgetFormElements\TraitTextarea;

}