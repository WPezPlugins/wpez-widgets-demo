<?php

namespace WPezWidgetsDemo\App\Plugin\Container\Contents;


class ClassGetValues {

	// Note: You can use the All trait, or use them individually and comment out the ones you're going to use for your widget

	// use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitAllGetValues;

	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetBool;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetMultiForOptions;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetNumber;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetValueElseDefault;
	use \WPezWidgetsDemo\App\Core\Traits\WidgetGetValues\TraitGetValueForOptions;

}