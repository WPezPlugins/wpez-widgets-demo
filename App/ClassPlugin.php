<?php

namespace WPezWidgetsDemo\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezWidgetsDemo\App\Plugin\Container\ClassContainer as Container;
use WPezWidgetsDemo\App\Core\HooksRegister\ClassHooksRegister as HooksRegister;
use WPezWidgetsDemo\App\Plugin\Widget\ClassWidgetsInit as WidgetsInit;


class ClassPlugin {

	protected $_new_hooks_reg;
	protected $_arr_actions;
	protected $_arr_filters;

    protected $_new_container;

    public function __construct() {

        $this->setPropertyDefaults();

	    $this->actions();

	    // this should be last
	    $this->hooksRegister();


    }


    protected function setPropertyDefaults() {

	    $this->_new_hooks_reg = new HooksRegister();
	    $this->_arr_actions   = [];
	    $this->_arr_filters   = [];

    	$this->_new_container = new Container();

    }

	public function actions( $bool = true ) {

		if ( $bool !== true ) {
			return;
		}

		$this->_arr_actions[] = [
			'active'    => true,
			'hook'      => 'widgets_init',
			'component' => new WidgetsInit($this->_new_container),
			'callback'  => 'registerWidget',
			'priority'  => 50
		];

	}

	protected function hooksRegister() {

		$this->_new_hooks_reg->loadActions( $this->_arr_actions );

		// $this->_new_hooks_reg->loadFilters( $this->_arr_filters );

		$this->_new_hooks_reg->actionRegister();

	}


}