## WPezPlugins: WPezWidgets Demo

__ More than just (another) boilerplate, this plugin demos a "micro-framework" for building WordPress widgets The ezWay.__

Less coding, more configuring. Less complexity, more consistency. Finally, the (ez) way you wish WP Core had implemented widgets originally.


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Install and activate the plugin; then add a WPezWidgets Demo widget somewhere. 

1 - Currently, the following field elements are supported out of the box:

- title
- chk_bool
- chk_multi
- number
- radio
- select
- textarea

https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Core/Traits/WidgetFormElements

You can see a configuration of each here:

https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Plugin/Container/Contents/ClassSettings.php

Notice how you won't be writing code for the actual form elements. You'll just need to define / configure / render them. 


2 - Much like the traditional approach, the WP_Widget class is extended

https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Core/WidgetAbstract/AbstractWPWidget.php

What's new (and imporved) is the form(), update() and widget() methods have been abstacted out, and they are now decoupled classes:

https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Core/WidgetAbstract/AbstractForm.php
https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Core/WidgetAbstract/AbstractUpdate.php
https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Core/WidgetAbstract/AbstractWidget.php

3 - The (demo) widget classes that extend the abstract classes are here:

https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Plugin/Widget

4 - Be sure to check out the form and update classes, as they use the form_element traits, as well as the get_values traits:

https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Core/Traits/WidgetFormElements

https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Core/Traits/WidgetGetValues

5 - Which are accessed via the container, as added here:

https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Plugin/Container/Contents

6 - Just prior to the register_widget() the classes for form, update and widget are injected into the main WPWidgetDemo class. 

7 - Once you've defined your form, and your update, you'll then need a frontend widget class. This is about the only thing that hasn't changed much. 

https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Plugin/Widget/ClassWidget.php

### FAQ

__1 - Why?__

I realized there was an ez'ier way to do widgets; a way that could make doing widgets less painful to more people. I wanted less reinventing of the wheel and more consistency.

__2 - Wow! Lots of abstraction.__

Yup. Exactly. 

__3- I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 




### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WidgetAbstract

- https://gitlab.com/wpezsuite/WPezTraits/WidgetFormElements

- https://gitlab.com/wpezsuite/WPezTraits/WidgetGetValues

- Or perhaps you'd prefer Tom McFarlin's WordPress-Widget-Boilerplate (https://github.com/tommcfarlin/WordPress-Widget-Boilerplate/tree/develop)?

### TODO

- Finish the validation in TraitGetNumber
- Support all HTML5 type="*" inputs
- Apply the same ideals to media form elements / widgets


### CHANGE LOG

- v0.0.1 - 4 April 2019
   
   UPDATED: README (no version change)

- v0.0.1 - 3 April 2019
   
   Hey! Ho!! Let's go!!! 

